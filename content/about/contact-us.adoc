+++
title = "Contact Us"
date = "2021-04-20"
aliases = [ "/contact-us/" ]
categories = [ "About" ]
summary = "Contact Information for the KiCad EDA Project"
[menu.main]
    parent = "About"
    name   = "Contact Us"
+++

{{< aboutlink "/img/about/kicad-logo.png" "/img/kicad_logo_paths.svg" >}}


[stripes,options="header",role="table table-striped table-condensed",frame=all]
|===
|Need to contact the KiCad Project?|

|Report a bug| https://go.kicad.org/bugs
|Technical Support| link:https://go.kicad.org/forum[Ask at the user forum] or
 
 link:/help/professional-support/[Purchase professional support]
|Register your plugin| plugins (at) kicad.org
|Become a Corporate Sponsor| sponsorship (at) kicad.org
|GDPR Issues| privacy (at) kicad.org
|Code of Conduct Concerns| abuse (at) kicad.org
|===


